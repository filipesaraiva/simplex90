! Programa método Simplex para disciplina de
! Modelos Lineares de Otimização
! @author: Filipe Saraiva
! @license: GPLv3

program simplex

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Simplex

	! Indica se o Problema é de
	! Máximização ou Minimização
	! 0 = Minimização; 1 = Maximização
	integer :: tipoOtimizacao

    ! Coeficientes das Restrições da
    ! Matriz de Produção (vetor "M")
    real, allocatable, dimension(:, :) :: matrizRestricoes

    ! Limites das Restrições da
    ! Matiz de Produção (vetor "b")
    real, allocatable, dimension(:) :: resultadoRestricoes

    ! Coeficientes da
    ! Função Objetivo
    real, allocatable, dimension(:) :: matrizObjetivo

    ! Resultado da Função Objetivo
    real :: resultadoObjetivo = 0.0

    ! Matriz com a Ordem das
    ! Variáveis na Base
    integer, allocatable, dimension(:) :: variaveisNaBase

    !###############################
    ! Variáveis Controle do Programa

    ! Variável para Alocar Número de
    ! Variáveis
    integer :: numVariaveis

    ! Variável para Alocar Número de
    ! Restrições
    integer :: numRestricoes

    ! Variável para Controle de Laço
    integer :: contador

    ! Variável para Controle de quem
    ! Entra na Base
    integer, dimension(1) :: variavelEntraNaBase

    ! Variável para Controle de quem
    ! Sai da Base
    integer :: restricaoSaiDaBase

    ! Variável que
    ! Sai da Base
    integer :: variavelSaiDaBase

    ! Variável para Controle se tem
    ! variável para entrar na base
    logical :: status

    ! Variável flag para
    ! Impressão dos Passos
    ! do Simplex
    integer :: imprimeQuadros

	!###############################
	! Fim da Declaração de Variáveis
	!###############################

	call system("clear")

	write(*, *) "Método Simplex para Otimização Linear"
	write(*, *) "--"

	do
		write(*, *) "Função Objetivo:"
		write(*, *) "0 - Minimização"
		write(*, *) "1 - Maximização"

		read(*, *) tipoOtimizacao

		if((tipoOtimizacao == 0) .or. (tipoOtimizacao == 1)) then
			exit
		end if

	end do

	write(*, *) "Número de Restrições: "
	read(*, *) numRestricoes

	write(*, *) "Número de Variáveis: "
	read(*, *) numVariaveis

	allocate(matrizRestricoes(numRestricoes, numVariaveis))
	allocate(resultadoRestricoes(numRestricoes))
	allocate(matrizObjetivo(numVariaveis))
	allocate(variaveisNaBase(numRestricoes))

	! @test
	! Teste para Verificar a Alocação de
	! Dimensões nas Matrizes
	! write(*, *) "Tamanho matrizRestricoes", size(matrizRestricoes)
	! write(*, *) "Tamanho resultadoRestricoes", size(resultadoRestricoes)
	! write(*, *) "Tamanho matrizObjetivo", size(matrizObjetivo)
	! write(*, *) "Tamanho variaveisNaBase", size(variaveisNaBase)

	call lerArquivo(numRestricoes, numVariaveis, matrizObjetivo, matrizRestricoes, resultadoRestricoes)

	! @test
	! Teste para Verificar a Alocação de
	! Valores nas Matrizes
	! write(*, *) "matrizObjetivo: ", matrizObjetivo
	! write(*, *) "matrizRestricoes: "
	!
	! do contador = 1, numRestricoes
	!	write(*, *) matrizRestricoes(contador, :)
	! end do
	!
	! write(*, *) "resultadoRestricoes: ", resultadoRestricoes

	call verificaVariaveisNaBase(numRestricoes, numVariaveis, matrizObjetivo, matrizRestricoes, resultadoRestricoes, &
		& variaveisNaBase, status)

	call escreveTableau(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, &
		& numRestricoes, variaveisNaBase)

	do
		write(*, *) "Imprimir Passos do Simplex:"
		write(*, *) "0 - Não"
		write(*, *) "1 - Sim, de Uma Vez"
		write(*, *) "2 - Sim, Passo-a-Passo"

		read(*, *) imprimeQuadros

		if((imprimeQuadros == 0) .or. (imprimeQuadros == 1) .or. (imprimeQuadros == 2)) then
			write(*, *) "=================================="
			exit
		end if

	end do

	if(status .eqv. .false.) then
		if(imprimeQuadros > 0) then
			write(*, *) "Método Big M."
		end if

		call metodoBigM(tipoOtimizacao, numRestricoes, numVariaveis, matrizObjetivo, matrizRestricoes, resultadoRestricoes, &
			& variaveisNaBase, resultadoObjetivo, imprimeQuadros)

		if(imprimeQuadros > 0) then
			call escreveTableau(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, &
				& numRestricoes, variaveisNaBase)

			if(imprimeQuadros == 2) then
				write(*, *)
				write(*, *) "Pressione *Enter* para Próximo Passo."
				call system("read")
			end if
		end if
	end if

	do
		call selecionaEntrarNaBase(tipoOtimizacao, matrizObjetivo, numVariaveis, variavelEntraNaBase, status)

		if(status .eqv. .false.) then
			exit
		end if

		call selecionaSairDaBase(matrizRestricoes, resultadoRestricoes, variavelEntraNaBase, restricaoSaiDaBase, numVariaveis, &
			& numRestricoes, status)

		if(status .eqv. .false.) then
			call escreveTableau(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, &
				& numRestricoes, variaveisNaBase)

			write(*, *)
			write(*, 200) variavelEntraNaBase
			write(*, *) "Problema Ilimitado."
			stop
		end if

		variavelSaiDaBase = variaveisNaBase(restricaoSaiDaBase)

		if(imprimeQuadros >= 1) then
			call escreveTableau(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, &
				& numRestricoes, variaveisNaBase)

			write(*, *)
			write(*, 200) variavelEntraNaBase
			write(*, 100) variaveisNaBase(restricaoSaiDaBase)

			if(imprimeQuadros == 2) then
				write(*, *)
				write(*, *) "Pressione *Enter* para Próximo Passo."
				call system("read")
			end if

		end if

		variaveisNaBase(restricaoSaiDaBase) = variavelEntraNaBase(1)

		if(status .eqv. .false.) then
			write(*, *) "Erro durante seleção da restrição a sair da base."
			write(*, *) "Abortando Execução do Programa."
			stop
		end if

		call pivoteamento(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, numRestricoes, &
			& variavelEntraNaBase, restricaoSaiDaBase)

	end do

	call imprimeResultado(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, numRestricoes, &
			& variavelEntraNaBase, restricaoSaiDaBase, variaveisNaBase)

	deallocate(matrizRestricoes)
	deallocate(resultadoRestricoes)
	deallocate(matrizObjetivo)
	deallocate(variaveisNaBase)

	100 format(" Variável X(", i2, ") sai da Base.")
	200 format(" Variável X(", i2, ") entra na Base.")

end program simplex

! Subrotina para Leitura
! do Arquivos com o Problema
subroutine lerArquivo(numRestricoes, numVariaveis, matrizObjetivo, matrizRestricoes, resultadoRestricoes)

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numRestricoes
    integer, intent(in) :: numVariaveis

    real, dimension(numVariaveis), intent(inout) :: matrizObjetivo
    real, dimension(numRestricoes), intent(inout) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(inout) :: matrizRestricoes

    !###############################
    ! Variáveis Locais

    ! Variável para Alocar os Status
    ! de Operações com Arquivos
    integer :: status

    ! String com Caminho do Arquivo
    character(len=100) :: caminhoArquivo

    ! Contador para Percorrer o Arquivo
    integer :: contador

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	write(*, *) "Caminho para o arquivo com o problema: "
	read(*, *) caminhoArquivo

	open(unit = 1, file = caminhoArquivo, status = "old", iostat = status)

	if(status /= 0) then
		write(*, *) "Erro ao Abrir Arquivo."
		write(*, *) "Terminando a Execução do Programa."
		stop
	end if

	! Alocação das Variáveis
	! da Função Objetivo
	read(1, *, iostat = status) matrizObjetivo(:)

	! Alocação das Variáveis matrizRestricoes
	! e resultadoRestricoes
	do contador = 1, numRestricoes
		read(1, *, iostat = status) matrizRestricoes(contador, :), resultadoRestricoes(contador)

		if(status /= 0) then
			write(*, *) "Erro ao Ler Arquivo."
			write(*, *) "Terminando a Execução do Programa."
			stop
		end if
	end do

	close(1)

end subroutine lerArquivo

! Subrotina para Verificar quais Variáveis
! estão na Base e se Precisará da Fase 1
subroutine verificaVariaveisNaBase(numRestricoes, numVariaveis, matrizObjetivo, matrizRestricoes, resultadoRestricoes, &
		& variaveisNaBase, status)

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numRestricoes
    integer, intent(in) :: numVariaveis

	integer, dimension(numRestricoes), intent(inout) :: variaveisNaBase
    real, dimension(numVariaveis), intent(inout) :: matrizObjetivo
    real, dimension(numRestricoes), intent(inout) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(inout) :: matrizRestricoes

    !###############################
    ! Variáveis Locais

    ! Verifica se a Resolução Precisará
    ! passar pela Fase 1 ou não
    integer :: status

    ! Contador dos Laços
    integer :: contador

    ! Contador de Alocação
    ! em variaveisNaBase
    integer :: contVariaveis = 1

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	do contador = 1, numVariaveis
		if(matrizObjetivo(contador) == 0) then
			if ((sum(matrizRestricoes(:, contador)) == 1) .and. (maxval(matrizRestricoes(:, contador)) == 1)) then
				variaveisNaBase(contVariaveis) = contador
				contVariaveis = contVariaveis + 1
			end if
		end if
	end do

	if(contVariaveis == (numRestricoes + 1)) then
		status = .true.
	else
		status = .false.
	end if

end subroutine verificaVariaveisNaBase

! Subrotina para Aplicar o
! Método Big M
subroutine metodoBigM(tipoOtimizacao, numRestricoes, numVariaveis, matrizObjetivo, matrizRestricoes, resultadoRestricoes, &
	& variaveisNaBase, resultadoObjetivo, imprimeQuadros)

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numRestricoes
    integer, intent(in) :: numVariaveis
    integer, intent(in) :: tipoOtimizacao
    integer, intent(in) :: imprimeQuadros

	integer, dimension(numRestricoes), intent(inout) :: variaveisNaBase
    real, dimension(numVariaveis), intent(inout) :: matrizObjetivo
    real, intent(inout) :: resultadoObjetivo
    real, dimension(numRestricoes), intent(inout) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(inout) :: matrizRestricoes

    !###############################
    ! Variáveis Locais

    integer :: numVariaveisBigM

    integer :: contador
    integer :: cont
    integer, dimension(1) :: variavelEntraNaBase
    integer :: variavelSaiDaBase
    integer :: restricaoSaiDaBase

    real :: penalidadeBigM

    real, dimension(numRestricoes, numRestricoes) :: matrizIdentidadeBigM

    real, allocatable, dimension(:) :: matrizObjetivoBigM
    real :: resultadoObjetivoBigM
    real, dimension(numRestricoes) :: resultadoRestricoesBigM
    real, allocatable, dimension(:, :) :: matrizRestricoesBigM

    logical :: status

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	numVariaveisBigM = numVariaveis + numRestricoes

	allocate(matrizObjetivoBigM(numVariaveisBigM))
	allocate(matrizRestricoesBigM(numRestricoes, numVariaveisBigM))

	do contador = 1, numRestricoes
		do cont = 1, numRestricoes
			if(contador /= cont) then
				matrizIdentidadeBigM(contador, cont) = 0
			else
				matrizIdentidadeBigM(contador, cont) = 1
			end if
		end do
	end do

	do contador = 1, numRestricoes
		do cont = 1, numVariaveisBigM
			matrizRestricoesBigM(contador, cont) = 0.0
		end do
	end do

	do contador = 1, numVariaveisBigM
		matrizObjetivoBigM(contador) = 0.0
	end do

	do contador = 1, numVariaveis
		matrizObjetivoBigM(contador) = matrizObjetivo(contador)
	end do

	do contador = 1, numRestricoes
		matrizRestricoesBigM(contador, :) = matrizRestricoes(contador, :)
	end do

	do contador = numVariaveis + 1, numVariaveisBigM
		matrizRestricoesBigM(:, contador) = matrizIdentidadeBigM(:, contador - numVariaveis)
		variaveisNaBase(contador - numVariaveis) = contador
		if (tipoOtimizacao == 0) then
			matrizObjetivoBigM(contador) = 1000000.0
		else
			matrizObjetivoBigM(contador) = -1000000.0
		end if
	end do

	do contador = 1, numRestricoes
		if (tipoOtimizacao == 0) then
			matrizObjetivoBigM(:) = matrizObjetivoBigM(:) + matrizRestricoesBigM(contador, :) * -1000000.0
			resultadoObjetivo = resultadoObjetivo + resultadoRestricoes(contador) * +1000000.0
		else
			matrizObjetivoBigM(:) = matrizObjetivoBigM(:) + matrizRestricoesBigM(contador, :) * 1000000.0
			resultadoObjetivo = resultadoObjetivo + resultadoRestricoes(contador) * -1000000.0
		end if
	end do

	call escreveTableau(matrizRestricoesBigM, matrizObjetivoBigM, resultadoRestricoes, resultadoObjetivo, numVariaveisBigM, &
		& numRestricoes, variaveisNaBase)

	do contador = 1, numRestricoes

		call selecionaEntrarNaBase(tipoOtimizacao, matrizObjetivoBigM, numVariaveisBigM, variavelEntraNaBase, status)

		if(status .eqv. .false.) then
			exit
		end if

		call selecionaSairDaBase(matrizRestricoesBigM, resultadoRestricoes, variavelEntraNaBase, restricaoSaiDaBase, &
			& numVariaveisBigM, numRestricoes, status)

		variavelSaiDaBase = variaveisNaBase(restricaoSaiDaBase)

		if(imprimeQuadros >= 1) then
			call escreveTableau(matrizRestricoesBigM, matrizObjetivoBigM, resultadoRestricoes, resultadoObjetivo, &
				& numVariaveisBigM, numRestricoes, variaveisNaBase)

			write(*, *)
			write(*, 200) variavelEntraNaBase
			write(*, 100) variaveisNaBase(restricaoSaiDaBase)

			if(imprimeQuadros == 2) then
				write(*, *)
				write(*, *) "Pressione *Enter* para Próximo Passo."
				call system("read")
			end if

		end if

		variaveisNaBase(restricaoSaiDaBase) = variavelEntraNaBase(1)

		if(status .eqv. .false.) then
			write(*, *) "Erro durante seleção da restrição a sair da base."
			write(*, *) "Abortando Execução do Programa."
			stop
		end if

		call pivoteamento(matrizRestricoesBigM, matrizObjetivoBigM, resultadoRestricoes, resultadoObjetivo, numVariaveisBigM, &
			& numRestricoes, variavelEntraNaBase, restricaoSaiDaBase)

	end do

	call escreveTableau(matrizRestricoesBigM, matrizObjetivoBigM, resultadoRestricoes, resultadoObjetivo, &
		& numVariaveisBigM, numRestricoes, variaveisNaBase)

	do contador = 1, numRestricoes
		do cont = 1, numVariaveis
			matrizRestricoes(contador, cont) = matrizRestricoesBigM(contador, cont)
		end do
	end do

	do contador = 1, numVariaveis
		matrizObjetivo(contador) = matrizObjetivoBigM(contador)
	end do

	write (*, *) "Fim do Método Big M."

	100 format(" Variável X(", i2, ") sai da Base.")
	200 format(" Variável X(", i2, ") entra na Base.")

	! write(*, *) "matrizObjetivoBigM: "
	! write(*, *) matrizObjetivoBigM
	! write(*, *) "matrizRestricoesBigM: "
	! do contador = 1, numRestricoes
	!	 write(*, *) matrizRestricoesBigM(contador, :)
	! end do
	! stop

end subroutine metodoBigM

! Subrotina para Verificar e
! Selecionar quem Entra
subroutine selecionaEntrarNaBase(tipoOtimizacao, matrizObjetivo, numVariaveis, variavelEntraNaBase, status)
	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numVariaveis
    integer, intent(in) :: tipoOtimizacao
    real, dimension(numVariaveis), intent(in) :: matrizObjetivo

    integer, dimension(1), intent(inout) :: variavelEntraNaBase
    logical, intent(inout) :: status

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	if(tipoOtimizacao == 0) then

		if(minval(matrizObjetivo) < 0) then
			variavelEntraNaBase = minloc(matrizObjetivo)
			status = .true.
		else
			status = .false.
		end if
	else

		if(maxval(matrizObjetivo) > 0) then
			variavelEntraNaBase = maxloc(matrizObjetivo)
			status = .true.
		else
			status = .false.
		end if

	end if

end subroutine selecionaEntrarNaBase

! Subrotina para Verificar e Selecionar
! a Restrição que sai da Base
subroutine selecionaSairDaBase(matrizRestricoes, resultadoRestricoes, variavelEntraNaBase, restricaoSaiDaBase, numVariaveis, &
	& numRestricoes, status)
	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numVariaveis
    integer, intent(in) :: numRestricoes
    integer, intent(in) :: variavelEntraNaBase

    real, dimension(numRestricoes), intent(in) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(in) :: matrizRestricoes

	integer, intent(inout) :: restricaoSaiDaBase

    logical, intent(inout) :: status

    !###############################
    ! Variáveis Locais

    ! Vetor para Alocar o Quociente
    ! Entre Resultado de Restrições
    ! e Vetor a entrar na Base
    real, dimension(numRestricoes) :: vetorQuociente

    ! Contador para Varrer os Vetores
    integer :: contador

    ! Menor Valor até o Momento
    ! Encontrado em vetorQuociente
    real :: menorValor

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	vetorQuociente(:) = resultadoRestricoes(:) / matrizRestricoes(:, variavelEntraNaBase)

	restricaoSaiDaBase = 0
	menorValor = 100000000000.00

	do contador = 1, numRestricoes

		if((matrizRestricoes(contador, variavelEntraNaBase) > 0) .and. (vetorQuociente(contador) >= 0) .and. &
			& (vetorQuociente(contador) < menorValor)) then

			menorValor = vetorQuociente(contador)
			restricaoSaiDaBase = contador
		end if

	end do

	if(restricaoSaiDaBase /= 0) then
		status = .true.
	else
		status = .false.
	end if

end subroutine selecionaSairDaBase

! Subrotina para Pivoteamento
! da matriz
subroutine pivoteamento(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, numRestricoes, &
	& variavelEntraNaBase, restricaoSaiDaBase)

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numVariaveis
    integer, intent(in) :: numRestricoes
    integer, intent(in) :: variavelEntraNaBase
    integer, intent(in) :: restricaoSaiDaBase

    real, dimension(numRestricoes), intent(inout) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(inout) :: matrizRestricoes
    real, dimension(numVariaveis), intent(inout) :: matrizObjetivo
    real, intent(inout) :: resultadoObjetivo

    !###############################
    ! Variáveis Locais

    ! Contador para varrer vetores
    integer :: contador

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	resultadoRestricoes(restricaoSaiDaBase) = resultadoRestricoes(restricaoSaiDaBase) / matrizRestricoes(restricaoSaiDaBase, &
		& variavelEntraNaBase)

	matrizRestricoes(restricaoSaiDaBase, :) = matrizRestricoes(restricaoSaiDaBase, :) / matrizRestricoes(restricaoSaiDaBase, &
		& variavelEntraNaBase)

	do contador = 1, numRestricoes
		if(contador == restricaoSaiDaBase) then
			cycle
		end if

		resultadoRestricoes(contador) = resultadoRestricoes(contador) + resultadoRestricoes(restricaoSaiDaBase) * &
			& (- matrizRestricoes(contador, variavelEntraNaBase))

		matrizRestricoes(contador, :) = matrizRestricoes(contador, :) + matrizRestricoes(restricaoSaiDaBase, :) * &
			& (- matrizRestricoes(contador, variavelEntraNaBase))

	end do

	resultadoObjetivo = resultadoObjetivo +  resultadoRestricoes(restricaoSaiDaBase) * (matrizObjetivo(variavelEntraNaBase))
	matrizObjetivo(:) = matrizObjetivo(:) +  matrizRestricoes(restricaoSaiDaBase, :) * (- matrizObjetivo(variavelEntraNaBase))

end subroutine pivoteamento

! Subrotina para Escrever
! o Tableau Simplex
subroutine escreveTableau(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, numRestricoes, &
	& variaveisNaBase)

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numVariaveis
    integer, intent(in) :: numRestricoes

    real, dimension(numRestricoes), intent(in) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(in) :: matrizRestricoes
    real, dimension(numVariaveis), intent(in) :: matrizObjetivo
    real, intent(in) :: resultadoObjetivo
    integer, dimension(numRestricoes), intent(in) :: variaveisNaBase


    !###############################
    ! Variáveis Locais

    ! Contador para varrer vetores
    integer :: contador

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	write(*, *) " "
	write(*, *) "--"
	write(*, *) "Funcao Objetivo:"
	write(*, 400, advance="no")
	write(*, *) matrizObjetivo, "= ", resultadoObjetivo
	write(*, *) " "
	write(*, *) "Tableau:"

	do contador = 1, numRestricoes
		write(*, 300, advance='no') variaveisNaBase(contador)
		write(*, *) matrizRestricoes(contador, :), "|", resultadoRestricoes(contador)
	end do

	300 format("   X(", i2,") | ")
	400 format("          ")

end subroutine escreveTableau

subroutine imprimeResultado(matrizRestricoes, matrizObjetivo, resultadoRestricoes, resultadoObjetivo, numVariaveis, numRestricoes, &
	& variavelEntraNaBase, restricaoSaiDaBase, variaveisNaBase)

	!###############################
	! Início Declaração de Variáveis
	!###############################
    implicit none

    !###############################
    ! Variáveis Parâmetros

    integer, intent(in) :: numVariaveis
    integer, intent(in) :: numRestricoes
    integer, intent(in) :: variavelEntraNaBase
    integer, intent(in) :: restricaoSaiDaBase

	integer, dimension(numRestricoes), intent(in) :: variaveisNaBase
    real, dimension(numRestricoes), intent(in) :: resultadoRestricoes
    real, dimension(numRestricoes, numVariaveis), intent(in) :: matrizRestricoes
    real, dimension(numVariaveis), intent(in) :: matrizObjetivo
    real, intent(in) :: resultadoObjetivo

    !###############################
    ! Variáveis Locais

    ! Contador para varrer vetores
    integer :: contador

    ! Vetor com o resultado ordenado das variáveis
    real, dimension(numVariaveis) :: vetorResultado

    ! Variável para contar o número de
    ! zeros e verificar se o problema
    ! tem infinitas soluções
    integer :: numZeros

    !###############################
	! Fim da Declaração de Variáveis
	!###############################

	write(*, *) " "
	write(*, *) " "
	write(*, *) "--"
	write(*, *) "Tableau Final"
	write(*, *) " "
	write(*, *) "Funcao Objetivo:"
	write(*, 400, advance="no")
	write(*, *) matrizObjetivo, "= ", resultadoObjetivo
	write(*, *) " "
	write(*, *) "Tableau Ótimo:"

	do contador = 1, numRestricoes
		write(*, 300, advance='no') variaveisNaBase(contador)
		write(*, *) matrizRestricoes(contador, :), "|", resultadoRestricoes(contador)
	end do

	do contador = 1, numVariaveis
		vetorResultado(contador) = 0
	end do

	do contador = 1, numRestricoes
		 vetorResultado(variaveisNaBase(contador)) = resultadoRestricoes(contador)
	end do

	write(*, *) " "
	write(*, *) "Resposta: "
	do contador = 1, numVariaveis
		write(*, 100) contador, vetorResultado(contador)
	end do

	write(*, 200) resultadoObjetivo

	numZeros = 0
	do contador = 1, numVariaveis
		if(vetorResultado(contador) == 0.0) then
			numZeros = numZeros + 1
		end if
	end do

	if(numZeros > numRestricoes) then
		write(*, *) "Problema com Infinitas Soluções."
	end if

	100 format("   X(", i2, ")*  =    ", f15.4)
	200 format("   Z*     =    ", f15.4)
	300 format("   X(", i2,") | ")
	400 format("          ")

end subroutine imprimeResultado
